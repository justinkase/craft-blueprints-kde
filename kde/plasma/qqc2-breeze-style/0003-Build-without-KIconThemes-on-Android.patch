From 9df322c9a9fe4b82053d355d00fdb7827877f9dc Mon Sep 17 00:00:00 2001
From: Nicolas Fella <nicolas.fella@gmx.de>
Date: Tue, 22 Jun 2021 21:19:13 +0200
Subject: [PATCH 3/3] Build without KIconThemes on Android

On Android we don't want to use KIconThemes. Linking against it actually breaks icon loading and nobody figured out why.

Instead fall back to QIcon::fromTheme like the default Kirigami impl.
---
 CMakeLists.txt                                      |  6 +++---
 kirigami-plasmadesktop-integration/CMakeLists.txt   |  3 +--
 .../plasmadesktoptheme.cpp                          | 13 +++++++++++--
 3 files changed, 15 insertions(+), 7 deletions(-)

diff --git a/CMakeLists.txt b/CMakeLists.txt
index 6d624d3..7a4e6a8 100644
--- a/CMakeLists.txt
+++ b/CMakeLists.txt
@@ -40,12 +40,12 @@ find_package(Qt5 ${REQUIRED_QT_VERSION} REQUIRED NO_MODULE COMPONENTS Core Gui Q
 
 if (NOT ANDROID)
     find_package(Qt5 ${REQUIRED_QT_VERSION} REQUIRED COMPONENTS DBus)
+    find_package(KF5IconThemes ${KF5_DEP_VERSION})
 endif()
 
 find_package(KF5 ${KF5_DEP_VERSION} REQUIRED COMPONENTS GuiAddons Config Kirigami2 CoreAddons)
 
 # IconThemes and ConfigWidgets are optional
-find_package(KF5IconThemes ${KF5_DEP_VERSION})
 find_package(KF5ConfigWidgets ${KF5_DEP_VERSION})
 
 if (NOT APPLE AND NOT WIN32)
@@ -99,8 +99,8 @@ add_definitions(-DKF_DISABLE_DEPRECATED_BEFORE_AND_AT=0x054900)
 
 add_subdirectory(style)
 
-if (KF5IconThemes_FOUND AND KF5WidgetsAddons_FOUND)
-add_subdirectory(kirigami-plasmadesktop-integration)
+if ((KF5IconThemes_FOUND OR ANDROID) AND KF5WidgetsAddons_FOUND)
+    add_subdirectory(kirigami-plasmadesktop-integration)
 endif(KF5IconThemes_FOUND AND KF5WidgetsAddons_FOUND)
 
 # FIXME: this assumes there will always be a QQC2 release for each Qt release, needs
diff --git a/kirigami-plasmadesktop-integration/CMakeLists.txt b/kirigami-plasmadesktop-integration/CMakeLists.txt
index 8a256fd..2201c98 100644
--- a/kirigami-plasmadesktop-integration/CMakeLists.txt
+++ b/kirigami-plasmadesktop-integration/CMakeLists.txt
@@ -16,9 +16,8 @@ target_link_libraries(org.kde.breeze
         Qt::Quick
         KF5::GuiAddons
         KF5::ConfigWidgets
-        KF5::IconThemes
 )
 
 if(NOT ANDROID)
-    target_link_libraries(org.kde.breeze PRIVATE Qt::DBus)
+    target_link_libraries(org.kde.breeze PRIVATE Qt::DBus KF5::IconThemes)
 endif()
diff --git a/kirigami-plasmadesktop-integration/plasmadesktoptheme.cpp b/kirigami-plasmadesktop-integration/plasmadesktoptheme.cpp
index bfc7f43..0010f18 100644
--- a/kirigami-plasmadesktop-integration/plasmadesktoptheme.cpp
+++ b/kirigami-plasmadesktop-integration/plasmadesktoptheme.cpp
@@ -9,7 +9,6 @@
 #include <KColorScheme>
 #include <KColorUtils>
 #include <KConfigGroup>
-#include <KIconLoader>
 #include <QDebug>
 #include <QGuiApplication>
 #include <QPalette>
@@ -20,7 +19,8 @@
 
 #ifndef Q_OS_ANDROID
 #include <QDBusConnection>
-#endif
+
+#include <KIconLoader>
 
 class IconLoaderSingleton
 {
@@ -31,6 +31,7 @@ public:
 };
 
 Q_GLOBAL_STATIC(IconLoaderSingleton, privateIconLoaderSelf)
+#endif
 
 class StyleSingleton : public QObject
 {
@@ -204,7 +205,10 @@ PlasmaDesktopTheme::PlasmaDesktopTheme(QObject *parent)
     // TODO: MOVE THIS SOMEWHERE ELSE
     m_lowPowerHardware = QByteArrayList{"1", "true"}.contains(qgetenv("KIRIGAMI_LOWPOWER_HARDWARE").toLower());
 
+    // We don't use KIconLoader on Android so we don't support recoloring there
+#ifndef Q_OS_ANDROID
     setSupportsIconColoring(true);
+#endif
 
     auto parentItem = qobject_cast<QQuickItem *>(parent);
     if (parentItem) {
@@ -271,6 +275,7 @@ void PlasmaDesktopTheme::syncFont()
 
 QIcon PlasmaDesktopTheme::iconFromTheme(const QString &name, const QColor &customColor)
 {
+#ifndef Q_OS_ANDROID
     QPalette pal = palette();
     if (customColor != Qt::transparent) {
         for (auto state : {QPalette::Active, QPalette::Inactive, QPalette::Disabled}) {
@@ -281,6 +286,10 @@ QIcon PlasmaDesktopTheme::iconFromTheme(const QString &name, const QColor &custo
     privateIconLoaderSelf->self.setCustomPalette(pal);
 
     return KDE::icon(name, &privateIconLoaderSelf->self);
+#else
+    // On Android we don't want to use the KIconThemes-based loader since that appears to be broken
+    return QIcon::fromTheme(name);
+#endif
 }
 
 void PlasmaDesktopTheme::syncColors()
-- 
2.32.0

